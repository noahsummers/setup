if on_mac ; then
  missing gtar  || alias  tar="gtar"
  missing gfind || alias find="gfind"
  missing ggrep || alias grep="ggrep"
  missing gsed  || alias  sed="gsed"
  missing gls   || alias   ls="gls -lvhG --group-directories-first --color=auto --time-style=iso"

  missing gindent || alias indent="gindent"

  alias history="history 0"
  alias cb="pbcopy"
else
  alias cb="xclip -selection c"
fi

alias ll="ls -a"

missing mawk || alias awk="mawk"

alias cl="clear"
alias cm="chezmoi"

alias dcrrm="docker-compose run --rm"
alias dcb="docker-compose build"
alias dcu="docker-compose up"
alias dcd="docker-compose down"
