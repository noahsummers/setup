setup_bash_completion () {
  # if bash_completion is not loaded
  if missing _completion_loader ; then # load it
    local POTENTIAL_BASH_COMPLETION_LOCATIONS=(
      "${HOMEBREW_PREFIX}/etc/bash_completion"
      "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh"
      "/usr/share/bash-completion/bash_completion"
    )

    for BASH_COMPLETION_SH in "${POTENTIAL_BASH_COMPLETION_LOCATIONS[@]}"; do
      if test -f $BASH_COMPLETION_SH ; then
        source $BASH_COMPLETION_SH
        break
      fi
    done
  fi

  if missing _completion_loader ; then
    not_found "bash_completion"
    return 1
  fi

  # if homebrew is installed and loaded
  if type brew &>/dev/null ; then

    # source the contents of the homebrew completions directory
    for COMPLETION in ${HOMEBREW_PREFIX}/etc/bash_completion.d/* ; do
      if test -f $COMPLETION ; then
        source "$COMPLETION"
      fi
    done

  fi
}

setup_bash_completion
