if missing git ; then
  not_found git
else
  g () {
    if [[ $# -gt 0 ]]; then
      git "$@"
    else
      git status
    fi
  }

  # ENABLE FULL BASH-COMPLETION FOR "G" ALIAS
  if type _completion_loader &>/dev/null ; then
    _completion_loader git
    complete -o bashdefault -o default -o nospace -F _git g
  elif type __git_complete &>/dev/null ; then
    __git_complete g _git
  fi
fi
