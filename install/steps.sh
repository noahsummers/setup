setup_install_brew () {
  if missing brew ; then
    /usr/bin/env bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  fi

  # source homebrew environment variables
  source <( curl -fsSL https://gitlab.com/noahsummers/setup/raw/master/dot_config/setup/brew.sh )

  if missing brew ; then
    echo "failed to install homebrew; exiting..."
    return 1
  fi
}

setup_install_bash () {
  if test ${BASH_VERSINFO:-0} -lt 5; then
    brew install bash \
      && echo ${HOMEBREW_PREFIX}/bin/bash | sudo tee -a /etc/shells \
      && chsh -s ${HOMEBREW_PREFIX}/bin/bash 
  fi
}

setup_install_asdf () {
  brew install \
    autoconf \
    automake \
    coreutils \
    curl \
    libtool \
    libxslt \
    libyaml \
    openssl \
    readline \
    unixodbc \
    unzip

  if [ ! -f ${HOME}/.asdf/asdf.sh ]; then
    git clone https://github.com/asdf-vm/asdf.git ${HOME}/.asdf
  fi

  cd ${HOME}/.asdf
  git reset --hard "$(git describe --abbrev=0 --tags)"
  cd -

  source ${HOME}/.asdf/asdf.sh

  if missing asdf ; then
    echo "failed to install asdf; exiting..."
    return 1
  fi
}

setup_install_heroku () {
  brew tap heroku/brew && brew install heroku
}

setup_install_tools () {
  brew install \
    coreutils \
    fd \
    findutils \
    fzf \
    gnu-indent \
    gnu-sed \
    gnu-tar \
    gnutls \
    grep \
    kakoune \
    lf \
    mawk \
    ripgrep \
    tealdeer \
    tree \
    tmux
}

setup_install_chezmoi () {
  if missing chezmoi ; then
    brew install twpayne/taps/chezmoi
  fi

  if missing chezmoi ; then
    echo "failed to install chezmoi; exiting..."
    return 1
  fi

  # init dotfiles from this repo
  chezmoi init https://gitlab.com/noahsummers/setup.git
}
